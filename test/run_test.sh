#!/bin/bash

set -e

cd $(dirname $0)

echo "> check of version"
cloc --version

echo "> use cloc"
rm -f result.json
cloc src --json --out=result.json
